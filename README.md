# Kinship_vue 

> Refactor the Kinship project with Vue.js. The Kinship project focuses on visualizing individual family tree.

## Before you begin:
We recommend you read about the basic building blocks that assemble a Kinship application:

* MongoDB
* Express
* Vue.js (2.0 or above)
* Node.js

**Prerequisites**

* Git
* Node.js
* MongoDB


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## File Structure
The folder structure is quite self-explanatory.

**src**

The src folder contains all frontend source code written in ES6.

Each sub-folder is a component, which includes at least three files: vue file used for component definition, js file for actions and html file for layout.

**build**

The build folder stores all files used for Webpack and backend.

**server**

This folder contains all backend (mainly for express router, mongoose.js) source code.

**config**

This folder contains all configure files used for Webpack.

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).