var path = require('path')
var express = require('express')
var config = require('../config')
var routers = require('../server/router')

// default port where dev server listens for incoming traffic
var port = process.env.PORT || config.build.port

// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
var proxyTable = config.dev.proxyTable

var app = express()

// proxy api requests
Object.keys(proxyTable).forEach(function(context) {
    var options = proxyTable[context]
    if (typeof options === 'string') {
        options = { target: options }
    }
    app.use(proxyMiddleware(context, options))
})

app.use('/', routers)

// serve pure static assets
app.use(express.static(path.join(__dirname, '../dist')));

app.listen(port, function(err) {
    if (err) {
        console.log(err)
        return
    }
    var uri = 'http://localhost:' + port
    console.log('Listening at ' + uri + '\n')
})
