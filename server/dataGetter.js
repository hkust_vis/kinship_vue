var Model = require('./db');

module.exports = {
    getRecords: function(name, respond) {
        var query = new RegExp(name)
        Model.originalModel.find({ name: query }, function(err, result, res) {
            if (err) return console.log(err);
            if (result) {
                respond.send(result);
            } else {
                respond.send(null)
            }
        });
    }
};
