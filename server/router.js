var router = require('express').Router();
var dataGetter = require('./dataGetter');
var converter=require('./STconverter.js')

router.get('/getRecords', function(req, res, next) {
    var name = req.query.name;
    var name_traditional=converter.tranStr(name, true)
    dataGetter.getRecords(name_traditional, res);
});
module.exports = router;
