'use strict'
var mongoose = require('mongoose')
    // link-MongoDB
mongoose.connect('mongodb://localhost/jinshenlu_query')

var db = mongoose.connection
db.on('error', console.error.bind(console, 'link fail'))
db.once('open', function() {
    console.log('link success');
})


var originalSchema = new mongoose.Schema({
    xing: String,
    ming: String,
}, {collection:'original', strict: false})

var originalModel = mongoose.model('original', originalSchema);

module.exports = {
    originalModel
};
