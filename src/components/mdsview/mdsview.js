import RangeSlider from 'vue-range-slider'
import { mdsviewConf } from '../../utils/config.js'
/* global d3 _ $ */

export default {
    name: 'mdsview',
    components: {
        RangeSlider,
    },
    data() {
        return {
            tableData: []
        }
    },
    methods: {
        onSearch: function(event) {
            var name = $('#searchContent').val()
            this.$http.get(`/getRecords?name=${name}`).then((res) => {
                this.tableData = res.data
                this.$store.commit('changeRecords', this.tableData)

                this.$nextTick()

            }, (err) => {
                console.log(err)
            })
        }
    },
}
