import VueTables from 'vue-tables-2'
import Vue from 'vue'
Vue.use(VueTables.client);

export default {
    name: 'tableview',
    components: {
        VueTables
    },
    data() {
        return {
            columns: ['name', 'xing', 'ming', 'diqu', 'jigou_1', 'jigou_2', 'jigou_3', 'guanzhi', 'year'],
            options: {
                sortable: ['name', 'xing', 'ming', 'diqu', 'jigou_1', 'jigou_2', 'jigou_3', 'guanzhi', 'year'],
                perPage: 25
            },
        }
    },
    computed: {
        tableData: function(){
        	return this.$store.getters.getRecords
        }
    }
}
