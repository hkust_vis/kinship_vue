// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './components/App/App'
import init from './utils/initialize.js'
import vueResource from 'vue-resource'
import store from './utils/store.js'

init();

Vue.use(vueResource);

new Vue({
    el: '#app',
    store,
    render: h => h(App)
})
