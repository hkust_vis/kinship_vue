export const mdsviewConf = {
    sliders: [{
        text: 'Num of Pos:',
        id: 'posnum',
        name: null,
        value: 0,
        min: 0,
        max: 100,
        step: 1,
        disabled: false
    }, {
        text: 'Timespan:',
        id: 'timespan',
        name: null,
        value: 0,
        min: 0,
        max: 100,
        step: 1,
        disabled: false
    }, {
        text: 'Num of Gen:',
        id: 'geno',
        name: null,
        value: 0,
        min: 0,
        max: 100,
        step: 1,
        disabled: false
    }, {
        text: 'Size:',
        id: 'size',
        name: null,
        value: 0,
        min: 0,
        max: 100,
        step: 1,
        disabled: false
    }],
    mds: {
        padding: 10,
        confBtn: [{
            id: 'region',
            highlightname: 'None',
            colorname: 'Region',
            color: ['rgb(31, 119, 180)', 'rgb(44, 160, 44)', 'rgb(214, 39, 40)', 'rgb(255, 127, 14)'],
            text: ['North', 'South Central', 'South', 'Central'],
            colorChecked: false,
            highlightChecked: true
        }, {
            id: 'position',
            highlightname: 'Has position',
            colorname: 'Position',
            color: ['#ffcc33', '#cccccc'],
            text: ['has position', 'no position'],
            colorChecked: false,
            highlightChecked: false
        }, {
            id: 'nonhan',
            colorname: 'Non-han',
            highlightname: 'Has non-han',
            color: ['#59b300', '#e5e600'],
            text: ['non-Han', 'Han'],
            colorChecked: false,
            highlightChecked: false
        }, {
            id: 'surname',
            colorname: 'Surname',
            highlightname: 'Has surname',
            color: ['#00e699', '#cccccc'],
            text: ['has surname', 'no surname'],
            colorChecked: false,
            highlightChecked: false
        }, {
            id: 'disabled',
            colorname: 'Disabled',
            highlightname: 'Is disabled',
            color: ['#ff1a1a', '#59b300'],
            text: ['disabled', 'not disabled'],
            colorChecked: false,
            highlightChecked: false
        }, {
            id: 'diminutive',
            colorname: 'Diminutive',
            highlightname: 'Has diminutive name',
            color: ['#ff3399', '#cccccc'],
            text: ['has diminutive name', 'no diminutive name'],
            colorChecked: false,
            highlightChecked: false
        }, {
            id: 'zuzhang',
            colorname: 'Zuzhang',
            highlightname: 'Is zuzhang',
            color: ['#996633', '#cccccc'],
            text: ['is zuzhang', 'not zuzhang'],
            colorChecked: false,
            highlightChecked: false
        }, {
            id: 'sex',
            colorname: 'Sex',
            highlightname: 'Is female',
            color: ['#d5779a', '#468eb6'],
            text: ['female', 'male'],
            colorChecked: true,
            highlightChecked: false
        }]
    }
}

export const treeviewConf = {
    nodeSize: 12,
    width: null,
    height: null,
    wifePadding: 30,
    river: {
        riverWidth: 100,
        margin: {
            top: 30,
            left: 50
        }
    }
}
