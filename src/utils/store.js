import Vue from 'vue'
import vuex from 'vuex'

Vue.use(vuex)

export default new vuex.Store({
    state: {
        records: []
    },
    mutations: {
        changeRecords(state, records) {
            state.records = records
        }
    },
    getters: {
        getRecords: state => state.records
    }
})
