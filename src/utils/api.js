import { treeviewConf } from './config.js'
/* global d3 */

export const dataProcessor = {
    treeView: {
        treeDataInit(nodes, links, rootwife) {
            var wife = { data: {} }
            var filterlinks = []
            var couplelinks = []

            // deal with root's wife
            if (rootwife) {
                wife.data = rootwife
                if (wife.data.birthyear < 0) {
                    wife.data.birthyear = nodes[0].data.birthyear
                }
            } else {
                wife.data.personid = 'none'
                nodes[0].data.w1id = 'none'
            }
            wife.x = nodes[0].x + treeviewConf.wifePadding
            wife.y = nodes[0].y
            wife.depth = 0
            wife.sex = 1
            nodes.push(wife)

            // get couple links
            couplelinks = []
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].data.sex === 1) continue

                for (var j = 0; j < nodes.length; j++) {
                    if (nodes[j].data.sex === 2) continue

                    if (nodes[i].data.w1id === nodes[j].data.personid || nodes[i].data.w2id === nodes[j].data.personid || nodes[i].data.personid === nodes[j].data.hid) {
                        couplelinks.push({
                            source: nodes[i],
                            target: nodes[j]
                        })
                        nodes[i].data.marry = 1
                        nodes[j].data.marry = 1
                        break
                    }
                }
            }

            // filter wife links
            filterlinks = []
            links.forEach(function(d) {
                if (d.data.sex === 2) filterlinks.push(d)
                else if (d.data.marry !== 1) filterlinks.push(d)
            })

            return {
                nodes: nodes,
                filterlinks: filterlinks,
                couplelinks: couplelinks
            }
        },
        getRiverData(nodes) {
            if (!nodes) return

            var pop = {}
            var male = {}
            var k, i

            var minYear = d3.min(nodes, function(d) {
                return d.data.birthyear
            })
            var maxYear = d3.max(nodes, function(d) {
                return d.data.birthyear + d.data.age
            })
            for (i = 0; i < nodes.length; i++) {
                var node = nodes[i]
                var birthyear, deadyear
                birthyear = node.data.birthyear
                deadyear = node.data.birthyear + node.data.age

                if (node.data.sex === 2) {
                    for (k = birthyear; k < deadyear; k++) {
                        male[k] ? male[k]++ : male[k] = 1
                    }
                }

                for (k = birthyear; k < deadyear; k++) {
                    pop[k] ? pop[k]++ : pop[k] = 1
                }
            }

            var maleArr = []
            var popArr = []
            for (i = minYear; i <= maxYear; i++) {
                maleArr.push({
                    date: i,
                    key: 'male',
                    value: male[i] ? male[i] : 0
                })
                popArr.push({
                    date: i,
                    key: 'pop',
                    value: pop[i] ? pop[i] : 0
                })
            }
            var popFirst = popArr[0]
            var maleFirst = maleArr[0]
            if (popFirst.value > 0) {
                popArr.unshift({
                    date: popFirst.date - 1,
                    key: 'pop',
                    value: 0
                })
                maleArr.unshift({
                    date: maleFirst.date - 1,
                    key: 'male',
                    value: 0
                })
            }
            var popLast = popArr[popArr.length - 1]
            var maleLast = maleArr[maleArr.length - 1]
            if (popFirst.value > 0) {
                popArr.push({
                    date: popLast.date + 1,
                    key: 'pop',
                    value: 0
                })
                maleArr.push({
                    date: maleLast.date + 1,
                    key: 'male',
                    value: 0
                })
            }
            return {
                population: popArr,
                male: maleArr
            }
        }
    }
}
